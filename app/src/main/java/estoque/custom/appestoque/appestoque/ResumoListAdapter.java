package estoque.custom.appestoque.appestoque;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResumoListAdapter extends ArrayAdapter<ProdutoResumo> {

    private Context mContext;
    private List<ProdutoResumo> produtosList = new ArrayList<>();

    public ResumoListAdapter(@NonNull Context context, @NonNull List<ProdutoResumo> lista) {
        super(context,0, lista);
        mContext = context;
        produtosList = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.row_res_list,parent,false);

        ProdutoResumo produtoAtual = produtosList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.nome_res_tv);
        name.setText(produtoAtual.getProduto());


        TextView data = (TextView) listItem.findViewById(R.id.qtd_res_tv);
        data.setText("Qtd: "+produtoAtual.getQtd());

        return listItem;
    }
}
