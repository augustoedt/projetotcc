package estoque.custom.appestoque.appestoque;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;

public class TransacoesActivity extends AppCompatActivity {

    ListView listView;
    InputStream inputStream;
    List<Produto> resultList;
    TransacoesListAdapter myAdapter;

    private AWSCredentialsProvider credentialsProvider;
    private AWSConfiguration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Conexão com AWS_Mobile
        AWSMobileClient.getInstance().initialize(this).execute();

        downloadWithTransferUtility();
        listView = findViewById(R.id.trans_list);
        listView.setAdapter(myAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(TransacoesActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

//faz o Download do arquivo csv de transações
    private void downloadWithTransferUtility() {

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();
        final File arquivoCSV = new File(this.getCacheDir(),"transactions_log.csv");
        TransferObserver downloadObserver =
                transferUtility.download(
                        "transactions_log.csv",
                        arquivoCSV);
        downloadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                    Log.v("Download: ", "Completed");
                    try {
//                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                        inputStream = new FileInputStream(arquivoCSV);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    resultList = read();
                    myAdapter = new TransacoesListAdapter(TransacoesActivity.this,resultList);
                    listView.setAdapter(myAdapter);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float)bytesCurrent/(float)bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d("MainActivity", "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == downloadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d("YourActivity", "Bytes Transferrred: " + downloadObserver.getBytesTransferred());
        Log.d("YourActivity", "Bytes Total: " + downloadObserver.getBytesTotal());
    }

    //Converte o arquivo csv em buffer para conversão em string retornando uma lista das transações lidas.
    public List read(){
        resultList = new ArrayList();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            int counter = 0;
            while ((csvLine = reader.readLine()) != null) {

                String[] row = csvLine.split(",");
                if(counter>0){
                    Produto produto = new Produto(row[0],row[1],row[2],row[3]);
                    resultList.add(produto);
                    Log.d("VariableTag", produto.getNome_produto()+" "+produto.getOp_produto());
                }
                counter++;

            }
            ((TextView)findViewById(R.id.data_mod)).setText("Ultima atualização: "+resultList.get(resultList.size()-1).getOp_data().split(" ")[0]);

        }
        catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: "+ex);
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: "+e);
            }
        }
        return resultList;
    }
}
