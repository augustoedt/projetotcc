package estoque.custom.appestoque.appestoque;


public class Produto {

    private String op_produto;
    private String nome_produto;
    private String op_data;
    private String tag_id;

    public Produto(String op_produto, String tag_id, String nome_produto, String op_data){
        setNome_produto(nome_produto);
        setOp_produto(op_produto);
        setOp_data(op_data);
        setTag_id(tag_id);

    }

    public String getOp_produto() {
        return op_produto;
    }

    public void setOp_produto(String op_produto) {
        this.op_produto = op_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getOp_data() {
        return op_data;
    }

    public void setOp_data(String op_data) {
        this.op_data = op_data;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }
}
