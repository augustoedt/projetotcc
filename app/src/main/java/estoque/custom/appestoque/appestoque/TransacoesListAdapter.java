package estoque.custom.appestoque.appestoque;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TransacoesListAdapter extends ArrayAdapter<Produto> {

    private Context mContext;
    private List<Produto> produtosList = new ArrayList<>();

    public TransacoesListAdapter(@NonNull Context context, @NonNull List<Produto> lista) {
        super(context,0, lista);
        mContext = context;
        produtosList = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.row_list,parent,false);

        Produto produtoAtual = produtosList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.nome_tv);
        name.setText(produtoAtual.getNome_produto());

        TextView operacao = (TextView) listItem.findViewById(R.id.qtd_tv);
        operacao.setText(produtoAtual.getOp_produto());

        TextView data = (TextView) listItem.findViewById(R.id.data_tv);
        String dataop[] = produtoAtual.getOp_data().split(" ");
        data.setText("Data da operação: "+
                dataop[0]+
                " as "+
                dataop[1].substring(0,dataop[1].length()-7));

        return listItem;
    }
}
