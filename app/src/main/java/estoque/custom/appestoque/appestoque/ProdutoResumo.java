package estoque.custom.appestoque.appestoque;

public class ProdutoResumo{


    private String produto;
    private String qtd;

    public ProdutoResumo(String produto, String qtd){
        setProduto(produto);
        setQtd(qtd);
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getQtd() {
        return qtd;
    }

    public void setQtd(String qtd) {
        this.qtd = qtd;
    }

}
