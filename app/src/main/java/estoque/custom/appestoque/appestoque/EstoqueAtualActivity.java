package estoque.custom.appestoque.appestoque;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EstoqueAtualActivity extends AppCompatActivity {
    ListView listView;
    InputStream inputStream;
    List<Produto> resultList;
    List<ProdutoResumo> resultListResume;
    ResumoListAdapter myAdapter;
    Map<String, Integer> contadorLista;
    Toolbar mToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_est_atual);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView)findViewById(R.id.est_atual_list);

        //inicializa conexão com AWSMobile
        AWSMobileClient.getInstance().initialize(this).execute();

        //FAz download o arquivo cvs
        downloadWithTransferUtility();

        //Preenche os dados da lista
        listView.setAdapter(myAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(EstoqueAtualActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
//Função reponsável por baixar o log no repositorio AWS-Amazon
    private void downloadWithTransferUtility() {
        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();
        final File arquivoCSV = new File(this.getCacheDir(),"transactions_log.csv");
        TransferObserver downloadObserver =
                transferUtility.download(
                        "transactions_log.csv",
                        arquivoCSV);
        downloadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                    Log.v("Download: ", "Completed");
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        ((TextView)findViewById(R.id.data_mod)).setText(sdf.format(arquivoCSV.lastModified()));
                        inputStream = new FileInputStream(arquivoCSV);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
//                    resultListResume = read();
                    myAdapter = new ResumoListAdapter(EstoqueAtualActivity.this,read());
                    listView.setAdapter(myAdapter);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float)bytesCurrent/(float)bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d("MainActivity", "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == downloadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d("YourActivity", "Bytes Transferrred: " + downloadObserver.getBytesTransferred());
        Log.d("YourActivity", "Bytes Total: " + downloadObserver.getBytesTotal());
    }
// Converte o arquivo CSV em um buffer de dados que é lido e convertido em
// string e faz um resumo todas das transações por produto
    public List read(){
        resultList = new ArrayList();
        contadorLista = new HashMap<String,Integer>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            int counter = 0;
            while ((csvLine = reader.readLine()) != null) {

                String[] row = csvLine.split(",");
                if(counter>0){
                    Produto produto = new Produto(row[0],row[1],row[2],row[3]);
                    resultList.add(produto);
                    Log.d("VariableTag", produto.getNome_produto()+" "+produto.getOp_produto());
                    Integer i = contadorLista.get(produto.getNome_produto());
                    if(i == null){
                        i=0;
                    }
                    if(produto.getOp_produto().equals("ADD")){
                        contadorLista.put(produto.getNome_produto(), i + 1);
                    }else if(produto.getOp_produto().equals("REMOVE")){
                        contadorLista.put(produto.getNome_produto(), i - 1);
                    }
                }
                counter++;

            }
            resultListResume = new ArrayList();
            for(Map.Entry<String, Integer> temp : contadorLista.entrySet()){
                ProdutoResumo produto =  new ProdutoResumo(temp.getKey(),String.valueOf(temp.getValue()));
                Log.v("resumo" ,temp.getKey()+" "+temp.getValue() );
                resultListResume.add(produto);
            }
            ((TextView)findViewById(R.id.data_mod)).setText("Ultima atualização: "+resultList.get(resultList.size()-1).getOp_data().split(" ")[0]);

        }
        catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: "+ex);
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: "+e);
            }
        }
        return resultListResume;
    }
}
